  $(function(){
    if (window.innerWidth <= 600) {
      return;
    }
    $("#mac-backdrop").on('click', function(){
      $("#mac-popup").slideUp('fast');
      $("#mac-backdrop").slideUp('fast');
    });
    $("#mac-popup .close-icon").on('click', function(){
      $("#mac-popup").slideUp('fast');
      $("#mac-backdrop").slideUp('fast');
    });
    if (window.location.search.indexOf('xmac=1') < 0) {
      if (window.navigator.platform.indexOf('Mac') >= 0) {
        $("#mac-backdrop").slideDown('slow');
        $("#mac-popup").slideDown('slow');
      }
    }
  });
