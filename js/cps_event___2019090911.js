
$(function(){
  if (window.dataLayer && Cookies.get('Cps3.user_session_id')) {
    window.dataLayer.push({
      'event': 'cps.js',
      'userId': Cookies.get('Cps3.user_session_id'),
      'sc': Cookies.get('Cps3.source_code'),
      'sid': Cookies.get('Cps3.user_session_id')
    });
  }
});
